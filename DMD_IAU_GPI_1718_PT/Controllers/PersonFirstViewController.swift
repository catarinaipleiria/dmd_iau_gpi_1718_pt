//
//  FirstViewController.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Catarina Silva on 21/09/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController, UITextFieldDelegate {
    
    var pessoa:Pessoa? = nil

    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBAction func confirmarPremido(_ sender: Any) {
        if pessoa == nil { //careful with !
            pessoa = Pessoa(nome: nomeTextField.text!, email: emailTextField.text!, idade: Int(ageTextField.text!)!)
        } else {
            pessoa!.nome = nomeTextField.text
            pessoa?.email = emailTextField.text
            pessoa?.idade = Int(ageTextField.text!)!
        }
        //print(pessoa!.nome)
        //print(pessoa!.email)
        
        //previous to archiving with PersonsRepository when [Person] was in the AppDelegate:
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.persons.append(pessoa!)
        
        Repositorio.repositorio.persons.append(pessoa!)
        Repositorio.repositorio.guardarPessoas()

        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var firstViewTopLabel: UILabel!
    
    @IBAction func firstViewButtonPressed(_ sender: UIButton) {
        firstViewTopLabel.text = "Já tocou no botão."
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let keyboardToolbar = UIToolbar()
        
        keyboardToolbar.sizeToFit() // ajustar ao ecrã
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            
                                            target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,
                                         target: self, action: #selector(doneClicked(_:)))
        
        keyboardToolbar.items = [flexBarButton, doneButton]
        
        ageTextField.inputAccessoryView = keyboardToolbar;
        
    }
    
    func doneClicked(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

