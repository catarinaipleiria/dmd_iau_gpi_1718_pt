//
//  MapViewController.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Catarina Silva on 12/10/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var temperaturaLabel: UILabel!
    let locationManager = CLLocationManager()
    
    var currentLocation = CLLocationCoordinate2D() //0,0
    var location:CLLocationCoordinate2D?
    
    var annotation = MKPointAnnotation()
    
    @IBOutlet weak var mapa: MKMapView!
    
    @IBAction func tapRecognised(_ sender: UITapGestureRecognizer) {
        let tapPoint = sender.location(in: mapa)
        location = mapa.convert(tapPoint, toCoordinateFrom: mapa)
        if let l = location {
            annotation.coordinate = l
            GeoNamesClient.getWeather(lat: l.latitude, lng: l.longitude, completion: { (weather, error) in
                
                OperationQueue.main.addOperation({
                    print(weather?.temperature)
                    self.temperaturaLabel.text = "Temperatura \(weather?.temperature)"
                })
            })
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 100 //kCLDistanceFilterNone
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
//        let span = MKCoordinateSpanMake(0.1, 0.1)
//        let region = MKCoordinateRegionMake(currentLocation, span)
//        mapa.region = region
        mapa.delegate = self
        mapa.showsUserLocation = true
        annotation.coordinate = currentLocation
        annotation.title = "Location"
        mapa.addAnnotation(annotation)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //LocationManagerDelegate Methods

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //if status == CLAuthorizationStatus.authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        //}
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        if let c = coord {
            print("latitude \(c.latitude) longitude \(c.longitude)")
            if (currentLocation.latitude != c.latitude) || (currentLocation.longitude != c.longitude){
                currentLocation = c
                mapa.region = MKCoordinateRegionMake(currentLocation, MKCoordinateSpanMake(0.1, 0.1))
                annotation.coordinate = c
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
