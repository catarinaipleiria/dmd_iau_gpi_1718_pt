//
//  PersonTableViewCell.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Luis Marcelino on 06/10/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    var person:Pessoa? {
        didSet {
            self.nomeLabel.text = person?.nome
            self.emailLabel.text = person?.email
            self.ageLabel.text = String(describing: person!.idade)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
