//
//  Pessoa.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Catarina Silva on 28/09/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import Foundation

class Pessoa:NSObject, NSCoding {
    var nome:String?
    var email:String?
    var idade:Int = 18
    
    init(nome:String, email:String, idade:Int) {
        self.nome = nome
        self.email = email
        self.idade = idade
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(nome, forKey: "nome")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(idade, forKey: "idade")
    }
    
    required init?(coder aDecoder: NSCoder) {
        nome = aDecoder.decodeObject(forKey: "nome") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        idade = aDecoder.decodeInteger(forKey: "idade")
    }
}
