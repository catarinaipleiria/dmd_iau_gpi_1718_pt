//
//  WeatherObs.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Luis Marcelino on 06/10/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import Foundation

struct WeatherObs {
    let stationName:String
    let lat:Double
    let lng:Double
    let temperature:Double
    let humidity:Double
    
    static func parseJson (json:[String:Any]) -> WeatherObs? {
        guard let lat = json["lat"] as? Double else {
            return nil
        }
        guard let lng = json["lng"] as? Double else {
            return nil
        }
        guard let temperatureStr = json["temperature"] as? String else {
            return nil
        }
        guard let temperature = Double(temperatureStr) else {
            return nil
        }
        guard let humidity = json["humidity"] as? Double else {
            return nil
        }
        guard let stationName = json["stationName"] as? String else {
            return nil
        }
        return WeatherObs(stationName: stationName, lat: lat, lng: lng, temperature: temperature, humidity: humidity)
    }
}
