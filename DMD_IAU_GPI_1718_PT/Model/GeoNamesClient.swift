//
//  GeoNamesClient.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Luis Marcelino on 13/10/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import Foundation

class GeoNamesClient {
    static func getWeather(lat:Double, lng:Double, completion: @escaping (_ weather:WeatherObs?, _ error:Error?)->Void ) {
        guard let url = URL(string:"http://api.geonames.org/findNearByWeatherJSON?lat=\(lat)&lng=\(lng)&username=luismarcelino") else {
            return
        }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, err) in
            if (err != nil || data == nil) {
                completion(nil, err)
                return
            }
            do {
                if let json = (try JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String:Any] {
                    let weatherJson = json["weatherObservation"] as! [String:Any]
                    let weather = WeatherObs.parseJson(json: weatherJson)
                    completion(weather, nil)
                }
            }
            catch {
                completion(nil,error)
            }
        }
        dataTask.resume()
    }
}
