//
//  Repositorio.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Catarina Silva on 02/11/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import Foundation

class Repositorio:ProtocoloRepositorio {
    
    static let repositorio = Repositorio()
    
    fileprivate init() {}
    
    var persons = [Pessoa]()
    
    var documentsPath:URL = URL(fileURLWithPath: "")
    var filePath:URL = URL(fileURLWithPath: "")
    
    //a chamar no inicio da app (didFinish)
    func obterPessoas () {
        //unarchive
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("People.data", isDirectory: false)
        let path = filePath.path
        
        print("loading persons from \(path)")
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Pessoa]{
            persons = newData
            print("Loaded \(persons.count) persons")
            //            print(newData[0].nome!)
            //            print(newData[0].email!)
            //            print(newData[0].idade)
        }
    }
    //a chamar sempre que houver alterações nas pessoas
    func guardarPessoas (){
        //archive
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("People.data", isDirectory: false)
        let path = filePath.path
        
        print("saving persons to \(path)")
        
        if NSKeyedArchiver.archiveRootObject(persons, toFile: path){
            print("success saving persons")
            print("Saved \(persons.count) persons")
        }
    }
    
    
    
}
