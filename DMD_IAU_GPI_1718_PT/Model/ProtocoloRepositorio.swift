//
//  ProtocoloRepositorio.swift
//  DMD_IAU_GPI_1718_PT
//
//  Created by Catarina Silva on 02/11/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import Foundation

protocol ProtocoloRepositorio{
    func obterPessoas ()
    func guardarPessoas()
}

