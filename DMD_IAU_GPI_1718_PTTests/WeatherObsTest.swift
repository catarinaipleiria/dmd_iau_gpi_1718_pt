//
//  WeatherObsTest.swift
//  DMD_IAU_GPI_1718_PTTests
//
//  Created by Luis Marcelino on 06/10/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import XCTest

class WeatherObsTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testParseJon() {
        let stringResponse = "{\"weatherObservation\":{\"elevation\":3,\"lng\":-9.033333333333333,\"observation\":\"LPAR 061000Z /////KT CAVOK 22/15 Q1019\",\"ICAO\":\"LPAR\",\"clouds\":\"clouds and visibility OK\",\"dewPoint\":\"15\",\"cloudsCode\":\"CAVOK\",\"datetime\":\"2017-10-06 10:00:00\",\"countryCode\":\"PT\",\"temperature\":\"22\",\"humidity\":64,\"stationName\":\"ALVERCA\",\"weatherCondition\":\"n/a\",\"hectoPascAltimeter\":1019,\"lat\":38.88333333333333}}";
        let responseData = stringResponse.data(using: .utf8)!
        let jsonResponse = try! JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as! [String:Any]
        let jsonWeather = jsonResponse["weatherObservation"] as! [String:Any]
        let weatherObs = WeatherObs.parseJson(json: jsonWeather)
        XCTAssertNotNil(weatherObs, "No weather response")
        XCTAssertEqual(weatherObs?.stationName, "ALVERCA")
        XCTAssertEqual(weatherObs?.temperature, 22)
        XCTAssertEqual(weatherObs?.lat, 38.88333333333333)
        XCTAssertEqual(weatherObs?.lng, -9.033333333333333)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
